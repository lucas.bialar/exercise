VERSION:=$(shell cat version)
REPO=repository.invitae.com/b3
PROJECT=conver_transcript
TAG=${REPO}/${PROJECT}:${VERSION}

delete:
	docker rm -f ${PROJECT} || true

build:delete
	docker build --rm -t ${TAG} .

run: build
	docker run -it ${TAG} /bin/bash

attach:
	docker exec -it ${PROJECT} /bin/bash
