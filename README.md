## Invitae Bioinformatics Exercise

### Assumptions

To run the script:
```
python convert.py transcripts.txt queries.txt
```

* Both the chromosome (reference) and the transcript coordinates are 0-based.

* The transcript is always mapped from genomic 5’ to 3’, so the other one was ignored.

* The files must have the names transcripts.txt and queries.txt 

* Others transcript and queries examples was created in order to test the solution.

* Since transcript file only provides transcript ids, it is assumed that one transcript aligns to a unique location on at most one chromosome. In other words, a transcript cannot align to 2 different chromosomes or to 2 different locations on a single chromosome.

* Not all the CIGAR operations is mapped and handled.

* In order to handle long CIGAR strings containing millions of alignment operations, for a very large number of alignments, external databases can be used to store and retrieve coordinate mappings between transcripts and chromosomes for each transcript-chromosome alignment. The libraries and "in memory" solution provided haven't a greate big O notation.

* The program is designed to check for check input files like missing columns and blank lines, with properly colum separated value.

* The program inspects the CIGAR strings for regex errors and presence of unknown characters.

* The project provide an Dockerfile and Makefile in order to make easy to deploy with a properly environment 

### Testing and error

* The transcripts file is inspected to check if it contains 4 columns not empty and if the third column contains an integer (alignment start coordinate). The CIGAR string is also inspected to check if it contains correct characters defined in the SAM/BAM document, and if it has a correct CIGAR format using provided regex. If any of these conditions are not satisfied, the program throws an appropriate message and exits.

* The queries file is inspected to check if it contains at least 2 columns and if the second column contains an integer. Also exists if not satisfied.

* If a query that was not present in input file 1, the program throws an appropriate warning and removed from result.

