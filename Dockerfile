FROM python:3.10.1-slim


# Application
RUN mkdir /app
WORKDIR /app

RUN apt-get update && apt-get install -y python3-dev build-essential less && apt-get clean 

RUN pip install pipenv

COPY . .
RUN pipenv install --system --deploy --ignore-pipfile

RUN python convert.py transcripts.txt queries.txt
