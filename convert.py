import re
import sys

import pandas as pd


def validate_transcript_file(transcript_file):
    # Arbitrary check if the name of the file is transcripts.txt
    if transcript_file == 'transcripts.txt':
        try:
            transcripts = pd.read_table(filepath_or_buffer=transcript_file,
                                        sep="\t", header=None,
                                        names=('tr_name', 'chr',
                                               'start_pos', 'cigar'),
                                        dtype={'tr_name': str, 'chr': str,
                                               'start_pos': int, 'cigar': str})
        except TypeError:
            print('Error: Transcript file shoud have 4 columns and tab separated \
                  columns with [STR, STR, INT, STR]')
            sys.exit()
        if transcripts.isnull().values.any():
            print('You have some miss formated value in transcript file or \
                  empty values')
            sys.exit()
        return transcripts
    else:
        print('The name of transcript file shoud be transcripts.txt')
        sys.exit()


def read_transcripts(transcript_file):
    # Read transcript files. Tab Separated and with no header
    transcripts = validate_transcript_file(transcript_file)
    cigar = dict()
    for i, transcript in transcripts.iterrows():
        cigar_value = transcript['cigar']
        transcript_name = transcript['tr_name']
        chr = transcript['chr']
        start_pos = transcript['start_pos']
        # Regex in samtools documentation about CIGAR strings
        cigar_string = re.findall('([0-9]+[MIDNSHPX=])', cigar_value)
        # Check the size of the matches is same to the size of the cigar string
        if len(''.join(cigar_string)) == len(cigar_value):
            # Add to a dictionary
            cigar[transcript_name] = dict()
            cigar[transcript_name]['cigar'] = cigar_string
            cigar[transcript_name]['chr'] = chr
            cigar[transcript_name]['start_pos'] = start_pos
        else:
            # Just print TR not valid
            print('TR \'{}\' transcripts is not valid'.format(transcript_name))
            sys.exit()

    return cigar


def validate_queries_file(queries_file):
    # Arbitrary check if the name of the file is queries.txt
    if queries_file == 'queries.txt':
        try:
            queries = pd.read_table(filepath_or_buffer=queries_file,
                                    sep="\t", header=None,
                                    names=('tr_name', 'tr_pos'),
                                    dtype={'tr_name': str, 'tr_pos': int})
        except TypeError:
            print('Error: Query file shoud have 2 columns and tab separated \
                columns with [STR, INT]')
            sys.exit()
        if queries.isnull().values.any():
            print('You have some miss formated value in query file or \
                empty values')
            sys.exit()
        return queries
    else:
        print('The name of query file shoud be queries.txt')
        sys.exit()


def validate_query_in_transcripts(qr, cigar):
    # Check if all tr_names from queries exists on transcript file
    if set(qr['tr_name'].values).issubset(cigar.keys()):
        print('All queries have transcripts')
    else:
        # Found different transcripts
        not_qr = set(qr['tr_name'].values).difference(cigar.keys())
        not_found = ','.join(not_qr)

        # Drop not found queries from queries dataframe in place
        print('Drop queries not found: {}'.format(not_found))
        queries.drop(queries[queries['tr_name'].isin(list(not_qr))].index,
                     inplace=True)


def read_queries(queries_file, cigar):

    # Read queries files. Tab Separated and with no header
    queries = validate_queries_file(queries_file)

    # Validate and remove queries no found in transcript file. In place result
    validate_query_in_transcripts(queries, cigar)

    return queries


def calculate_position(queries, cigar):

    # Iterate queries file
    output = []
    for j, query in queries.iterrows():
        # CIGAR calc
        cigar_tr = cigar[query['tr_name']]['cigar']
        qr_position = query['tr_pos']

        # Get Transcription line and values
        transcription = cigar[query['tr_name']]
        chro = transcription['chr']
        start_position = transcription['start_pos']
        size = 0
        chr_size = 0
        for cigar_unit in cigar_tr:
            cigar_op = cigar_unit[-1]
            cigar_size = int(cigar_unit[:-1])
            # Ignoring some operation from samtools documentation
            if cigar_op in ['M', 'I', '=', 'X', 'S']:
                size = size + cigar_size
            chr_size = chr_size + cigar_size
            if qr_position < size:
                chr_position = start_position + qr_position + chr_size - size
                break
        # Add output response in a list
        output.append((query['tr_name'], query['tr_pos'], chro, chr_position))
    return output


if __name__ == "__main__":

    # Load files
    try:
        transcript_file = sys.argv[1]
        queries_file = sys.argv[2]
    except IndexError:
        print('You must provide the name of transcript and query files')
        sys.exit()

    # Read transcript file
    cigar = read_transcripts(transcript_file)

    # Read queries file
    queries = read_queries(queries_file, cigar)

    # Generate positions and final format of the file
    output = calculate_position(queries, cigar)

    # Convert into dataframe only to export
    df_output = pd.DataFrame.from_records(output)
    # Export in txt file using \t as a separator and no header
    df_output.to_csv(path_or_buf='output.txt', sep='\t',
                     header=None, index=False)
    print('All Done')
